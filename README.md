#Install

```
yarn add https://git.profects.com/profects/react-api.git
```

#GET

```
import api from 'react-api'

api({
    path: 'client/company',
    params: {
        param1: 1,
    },

})
```

#POST/PUT

```
import api from 'react-api'

api({
    path: 'client/company',
    body: {
        {
            id: 1,
            name: 'UpdatedName',
        }
    },

})
```

#All options

Disable cache via
REACT_APP_DISABLE_CACHE=true

```
import api from 'react-api'

api({
    path: 'client/company',
    basePath: '' // change default base url of api if you want to fetch other api
    method: 'GET', //default
    body: {}, // when PUT or POST
    type: 'json',// text, headers
    params: {
        param1: 1,
        param2: 'dd'
    },
    headers:{

    },

    token: '', // use react-auth profects fetch middleware to automatically insert this token
    ignoreStatus: null, // Could be 401 e.g. to prevent promise error when account is not complete
    ignoreCache: false,
})
```

## In redux-saga

```
yield call(api, 'client', {
    ...etc,
})
```
