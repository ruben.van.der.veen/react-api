// connect to own proxy to prevent OPTIONS requests and CORS issues
var getLocalUrl = function getLocalUrl() {
  return (// eslint-disable-next-line
    location.protocol + "//" + location.hostname + (location.port ? ":" + location.port : "")
  );
};

function getApiEndpoint() {
  return process.env.REACT_APP_API_ENDPOINT || process.env.NEXT_PUBLIC_API_ENDPOINT;
}

function getDexEndpoint() {
  return process.env.REACT_APP_DEX_ENDPOINT || process.env.NEXT_PUBLIC_DEX_ENDPOINT;
}

export var getEndpoint = function getEndpoint(type) {
  var apiUrl = getApiEndpoint();
  var dexUrl = getDexEndpoint();

  if (type === "api" && apiUrl) {
    return apiUrl + "/";
  }

  if (type === "dex" && dexUrl) {
    return dexUrl + "/";
  }

  return getLocalUrl() + "/" + type + "/";
};