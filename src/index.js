import qs from "qs";
import { getEndpoint } from "./endpoint";
import Promise from "./promise";

export const defaultPath = getEndpoint("api");
export const authPath = getEndpoint("dex");

const jsonHeaders = {
  Accept: "application/json",
  "Content-Type": "application/json"
};
const ignoreCacheHeaders = {
  "Cache-Control": "no-cache"
};

export const pureFetch = (fullPath, options, type, ignoreStatus) => {
  return new Promise((resolve, reject) =>
    fetch(fullPath, options)
      .then(response => {
        if (!response.ok && response.status !== ignoreStatus) {
          throw response;
        }
        switch (type) {
          case "text": {
            const textResponse = response.text();
            return textResponse;
          }
          case "headers":
            const headers = response.headers;
            return headers;
          case "blob":
            const blobResponse = response.blob();
            return blobResponse;
          default: {
            const jsonResponse = response.json();
            return jsonResponse;
          }
        }
      })
      .then(content => {
        resolve(content);
      })
      .catch(error => {
        reject(error);
      })
  );
};

export default ({
  path,
  params,
  method = "GET",
  body,
  headers,
  type,
  basePath,
  token,
  ignoreStatus,
  ignoreCache = process.env.REACT_APP_DISABLE_CACHE === "true"
}) => {
  const fullPath =
    (basePath || defaultPath) +
    path +
    (params ? `?${qs.stringify(params, { arrayFormat: "brackets" })}` : "");

  const options = {
    method
    //headers: {},
  };

  // default
  if (!type) {
    options.headers = jsonHeaders;
  }
  if (headers) {
    options.headers = headers;
  }
  if (ignoreCache) {
    options.headers = { ...options.headers, ...ignoreCacheHeaders };
  }
  if (token) {
    options.headers = { ...options.headers, Authorization: `Bearer ${token}` };
  }

  // post/put data
  if (body) {
    if (body instanceof FormData) {
      options.body = body;
    } else {
      options.body = JSON.stringify(body);
    }
  }

  return pureFetch(fullPath, options, type, ignoreStatus);
};
