// connect to own proxy to prevent OPTIONS requests and CORS issues
const getLocalUrl = () =>
  // eslint-disable-next-line
  `${location.protocol}//${location.hostname}${
    location.port ? `:${location.port}` : ""
  }`;

function getApiEndpoint() {
  return (
    process.env.REACT_APP_API_ENDPOINT || process.env.NEXT_PUBLIC_API_ENDPOINT
  );
}

function getDexEndpoint() {
  return (
    process.env.REACT_APP_DEX_ENDPOINT || process.env.NEXT_PUBLIC_DEX_ENDPOINT
  );
}

export const getEndpoint = (type) => {
  const apiUrl = getApiEndpoint();
  const dexUrl = getDexEndpoint();

  if (type === "api" && apiUrl) {
    return `${apiUrl}/`;
  }

  if (type === "dex" && dexUrl) {
    return `${dexUrl}/`;
  }

  return `${getLocalUrl()}/${type}/`;
};
