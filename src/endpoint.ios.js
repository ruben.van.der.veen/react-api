import Config from "react-native-config";

function getApiEndpoint() {
  return Config.REACT_APP_API_ENDPOINT || Config.NEXT_PUBLIC_API_ENDPOINT;
}

function getDexEndpoint() {
  return Config.REACT_APP_DEX_ENDPOINT || Config.NEXT_PUBLIC_DEX_ENDPOINT;
}

export const getEndpoint = (type) => {
  const apiUrl = getApiEndpoint();
  const dexUrl = getDexEndpoint();

  if (type === "api" && apiUrl) {
    return `${apiUrl}/`;
  }

  if (type === "dex" && dexUrl) {
    return `${dexUrl}/`;
  }
  return `PLEASE SET REACT_APP_DEX_ENDPOINT and REACT_APP_DEX_ENDPOINT `;
};
