"use strict";

exports.__esModule = true;
exports.getEndpoint = void 0;

var _reactNativeConfig = _interopRequireDefault(require("react-native-config"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function getApiEndpoint() {
  return _reactNativeConfig["default"].REACT_APP_API_ENDPOINT || _reactNativeConfig["default"].NEXT_PUBLIC_API_ENDPOINT;
}

function getDexEndpoint() {
  return _reactNativeConfig["default"].REACT_APP_DEX_ENDPOINT || _reactNativeConfig["default"].NEXT_PUBLIC_DEX_ENDPOINT;
}

var getEndpoint = function getEndpoint(type) {
  var apiUrl = getApiEndpoint();
  var dexUrl = getDexEndpoint();

  if (type === "api" && apiUrl) {
    return apiUrl + "/";
  }

  if (type === "dex" && dexUrl) {
    return dexUrl + "/";
  }

  return "PLEASE SET REACT_APP_DEX_ENDPOINT and REACT_APP_DEX_ENDPOINT ";
};

exports.getEndpoint = getEndpoint;