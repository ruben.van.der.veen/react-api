"use strict";

exports.__esModule = true;
exports["default"] = exports.pureFetch = exports.authPath = exports.defaultPath = void 0;

var _qs = _interopRequireDefault(require("qs"));

var _endpoint = require("./endpoint");

var _promise = _interopRequireDefault(require("./promise"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var defaultPath = (0, _endpoint.getEndpoint)("api");
exports.defaultPath = defaultPath;
var authPath = (0, _endpoint.getEndpoint)("dex");
exports.authPath = authPath;
var jsonHeaders = {
  Accept: "application/json",
  "Content-Type": "application/json"
};
var ignoreCacheHeaders = {
  "Cache-Control": "no-cache"
};

var pureFetch = function pureFetch(fullPath, options, type, ignoreStatus) {
  return new _promise["default"](function (resolve, reject) {
    return fetch(fullPath, options).then(function (response) {
      if (!response.ok && response.status !== ignoreStatus) {
        throw response;
      }

      switch (type) {
        case "text":
          {
            var textResponse = response.text();
            return textResponse;
          }

        case "headers":
          var headers = response.headers;
          return headers;

        case "blob":
          var blobResponse = response.blob();
          return blobResponse;

        default:
          {
            var jsonResponse = response.json();
            return jsonResponse;
          }
      }
    }).then(function (content) {
      resolve(content);
    })["catch"](function (error) {
      reject(error);
    });
  });
};

exports.pureFetch = pureFetch;

var _default = function _default(_ref) {
  var path = _ref.path,
      params = _ref.params,
      _ref$method = _ref.method,
      method = _ref$method === void 0 ? "GET" : _ref$method,
      body = _ref.body,
      headers = _ref.headers,
      type = _ref.type,
      basePath = _ref.basePath,
      token = _ref.token,
      ignoreStatus = _ref.ignoreStatus,
      _ref$ignoreCache = _ref.ignoreCache,
      ignoreCache = _ref$ignoreCache === void 0 ? process.env.REACT_APP_DISABLE_CACHE === "true" : _ref$ignoreCache;
  var fullPath = (basePath || defaultPath) + path + (params ? "?" + _qs["default"].stringify(params, {
    arrayFormat: "brackets"
  }) : "");
  var options = {
    method: method //headers: {},

  }; // default

  if (!type) {
    options.headers = jsonHeaders;
  }

  if (headers) {
    options.headers = headers;
  }

  if (ignoreCache) {
    options.headers = _extends({}, options.headers, ignoreCacheHeaders);
  }

  if (token) {
    options.headers = _extends({}, options.headers, {
      Authorization: "Bearer " + token
    });
  } // post/put data


  if (body) {
    if (body instanceof FormData) {
      options.body = body;
    } else {
      options.body = JSON.stringify(body);
    }
  }

  return pureFetch(fullPath, options, type, ignoreStatus);
};

exports["default"] = _default;