"use strict";

exports.__esModule = true;
exports["default"] = void 0;

var _bluebird = _interopRequireDefault(require("bluebird"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

_bluebird["default"].config({
  cancellation: true
});

var _default = _bluebird["default"];
exports["default"] = _default;
module.exports = exports.default;